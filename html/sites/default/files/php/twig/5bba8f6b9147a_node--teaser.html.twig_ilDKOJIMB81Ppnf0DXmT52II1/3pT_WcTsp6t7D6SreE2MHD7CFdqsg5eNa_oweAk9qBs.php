<?php

/* node--teaser.html.twig */
class __TwigTemplate_f10e2cdf364e797187f6e0fb66dca3245ff453e7f2396406d4947ba8b9e21090 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'card_teaser_type' => array($this, 'block_card_teaser_type'),
            'card_image' => array($this, 'block_card_image'),
            'card_body' => array($this, 'block_card_body'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array("set" => 73, "if" => 85, "block" => 94, "trans" => 167, "embed" => 129);
        $filters = array("clean_class" => 98);
        $functions = array("attach_library" => 69);

        try {
            $this->env->getExtension('Twig_Extension_Sandbox')->checkSecurity(
                array('set', 'if', 'block', 'trans', 'embed'),
                array('clean_class'),
                array('attach_library')
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 68
        echo "
";
        // line 69
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->env->getExtension('Drupal\Core\Template\TwigExtension')->attachLibrary("socialbase/teaser"), "html", null, true));
        echo "
";
        // line 70
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->env->getExtension('Drupal\Core\Template\TwigExtension')->attachLibrary("socialbase/page-node"), "html", null, true));
        echo "

";
        // line 73
        $context["classes"] = array(0 => "teaser", 1 => (((        // line 75
($context["view_mode"] ?? null) == "teaser")) ? ("card") : ("")), 2 => (((        // line 76
($context["view_mode"] ?? null) == "activity")) ? ("teaser--stream") : ("")), 3 => (((        // line 77
($context["view_mode"] ?? null) == "activity_comment")) ? ("teaser--stream") : ("")), 4 => (($this->getAttribute(        // line 78
($context["node"] ?? null), "isPromoted", array(), "method")) ? ("promoted") : ("")), 5 => (($this->getAttribute(        // line 79
($context["node"] ?? null), "isSticky", array(), "method")) ? ("sticky") : ("")), 6 => (( !$this->getAttribute(        // line 80
($context["node"] ?? null), "isPublished", array(), "method")) ? ("teaser--unpublished") : ("")));
        // line 83
        echo "
<div";
        // line 84
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["attributes"] ?? null), "addClass", array(0 => ($context["classes"] ?? null)), "method"), "html", null, true));
        echo ">
  ";
        // line 85
        if (($context["status_label"] ?? null)) {
            // line 86
            echo "    <div class=\"node--unpublished__label\">
      ";
            // line 87
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["status_label"] ?? null), "html", null, true));
            echo "
    </div>
  ";
        }
        // line 90
        echo "
  <div class='teaser__image'>

    ";
        // line 93
        if ((($context["view_mode"] ?? null) == "teaser")) {
            // line 94
            echo "      ";
            $this->displayBlock('card_teaser_type', $context, $blocks);
            // line 103
            echo "    ";
        }
        // line 104
        echo "
    ";
        // line 106
        echo "    ";
        $this->displayBlock('card_image', $context, $blocks);
        // line 113
        echo "

  </div>

  <div class='teaser__body'>
    <div class=\"teaser__content\">
      ";
        // line 119
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["title_prefix"] ?? null), "html", null, true));
        echo "
      ";
        // line 120
        if ( !($context["page"] ?? null)) {
            // line 121
            echo "        <h4";
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["title_attributes"] ?? null), "html", null, true));
            echo " class=\"teaser__title\">
            <a href=\"";
            // line 122
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["url"] ?? null), "html", null, true));
            echo "\" rel=\"bookmark\">";
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["label"] ?? null), "html", null, true));
            echo "</a>
        </h4>
      ";
        }
        // line 125
        echo "      ";
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["title_suffix"] ?? null), "html", null, true));
        echo "

      ";
        // line 127
        $this->displayBlock('card_body', $context, $blocks);
        // line 151
        echo "    </div>

    <div class=\"card__actionbar\">

        ";
        // line 155
        if ((($context["comment_count"] ?? null) > 0)) {
            // line 156
            echo "          <a href=\"";
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["url"] ?? null), "html", null, true));
            echo "#section-comments\" class=\"badge badge--pill badge-default teaser__badge\">
            <span class=\"badge__container\">
              <svg class=\"badge__icon\">
                <use xlink:href=\"#icon-comment\"></use>
              </svg>
              <span class=\"badge__label\">";
            // line 161
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["comment_count"] ?? null), "html", null, true));
            echo "</span>
            </span>
          </a>
        ";
        }
        // line 165
        echo "
        ";
        // line 166
        if ((($context["visibility_icon"] ?? null) && ($context["visibility_label"] ?? null))) {
            // line 167
            echo "          <div class=\"badge teaser__badge\" title=\"";
            echo t("The visibility of this content is set to", array());
            echo " ";
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["visibility_label"] ?? null), "html", null, true));
            echo "\">
              <svg class=\"badge__icon\">
                <use xlink:href=\"#icon-";
            // line 169
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["visibility_icon"] ?? null), "html", null, true));
            echo "\"></use>
              </svg>
          </div>
        ";
        }
        // line 173
        echo "
        ";
        // line 174
        if ($this->getAttribute(($context["content"] ?? null), "enrolled", array())) {
            // line 175
            echo "          <span class=\"badge badge-secondary teaser__badge\">";
            // line 176
            echo t("Enrolled", array());
            // line 177
            echo "</span>
        ";
        }
        // line 179
        echo "
      ";
        // line 180
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["content"] ?? null), "links", array()), "html", null, true));
        echo "
    </div>

  </div>

</div>

";
        // line 187
        if (((($context["view_mode"] ?? null) == "activity") && $this->getAttribute(($context["content"] ?? null), "field_book_comments", array()))) {
            // line 188
            echo "  ";
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["content"] ?? null), "field_book_comments", array()), "html", null, true));
            echo "
";
        }
    }

    // line 94
    public function block_card_teaser_type($context, array $blocks = array())
    {
        // line 95
        echo "
        <div class=\"teaser__teaser-type\">
          <svg class=\"teaser__teaser-type-icon\">
            <use xlink:href=\"#icon-";
        // line 98
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, \Drupal\Component\Utility\Html::getClass($this->getAttribute(($context["node"] ?? null), "bundle", array())), "html", null, true));
        echo "\"></use>
          </svg>
        </div>

      ";
    }

    // line 106
    public function block_card_image($context, array $blocks = array())
    {
        // line 107
        echo "      ";
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["content"] ?? null), "field_event_image", array()), "html", null, true));
        echo "
      ";
        // line 108
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["content"] ?? null), "field_topic_image", array()), "html", null, true));
        echo "
      ";
        // line 109
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["content"] ?? null), "field_page_image", array()), "html", null, true));
        echo "
      ";
        // line 110
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["content"] ?? null), "field_book_image", array()), "html", null, true));
        echo "
      ";
        // line 111
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["content"] ?? null), "field_image", array()), "html", null, true));
        echo "
    ";
    }

    // line 127
    public function block_card_body($context, array $blocks = array())
    {
        // line 128
        echo "
        ";
        // line 129
        $this->loadTemplate("node--teaser.html.twig", "node--teaser.html.twig", 129, "1911505782")->display($context);
        // line 138
        echo "
        ";
        // line 139
        if ($this->getAttribute(($context["content"] ?? null), "group_name", array())) {
            // line 140
            echo "          ";
            $this->loadTemplate("node--teaser.html.twig", "node--teaser.html.twig", 140, "1071370171")->display($context);
            // line 144
            echo "        ";
        }
        // line 145
        echo "
        ";
        // line 146
        $this->loadTemplate("node--teaser.html.twig", "node--teaser.html.twig", 146, "1614396130")->display($context);
        // line 149
        echo "
      ";
    }

    public function getTemplateName()
    {
        return "node--teaser.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  285 => 149,  283 => 146,  280 => 145,  277 => 144,  274 => 140,  272 => 139,  269 => 138,  267 => 129,  264 => 128,  261 => 127,  255 => 111,  251 => 110,  247 => 109,  243 => 108,  238 => 107,  235 => 106,  226 => 98,  221 => 95,  218 => 94,  210 => 188,  208 => 187,  198 => 180,  195 => 179,  191 => 177,  189 => 176,  187 => 175,  185 => 174,  182 => 173,  175 => 169,  167 => 167,  165 => 166,  162 => 165,  155 => 161,  146 => 156,  144 => 155,  138 => 151,  136 => 127,  130 => 125,  122 => 122,  117 => 121,  115 => 120,  111 => 119,  103 => 113,  100 => 106,  97 => 104,  94 => 103,  91 => 94,  89 => 93,  84 => 90,  78 => 87,  75 => 86,  73 => 85,  69 => 84,  66 => 83,  64 => 80,  63 => 79,  62 => 78,  61 => 77,  60 => 76,  59 => 75,  58 => 73,  53 => 70,  49 => 69,  46 => 68,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "node--teaser.html.twig", "profiles/contrib/social/themes/socialbase/templates/node/node--teaser.html.twig");
    }
}


/* node--teaser.html.twig */
class __TwigTemplate_f10e2cdf364e797187f6e0fb66dca3245ff453e7f2396406d4947ba8b9e21090_1911505782 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 129
        $this->parent = $this->loadTemplate("node--teaser__field.html.twig", "node--teaser.html.twig", 129);
        $this->blocks = array(
            'field_icon' => array($this, 'block_field_icon'),
            'field_value' => array($this, 'block_field_value'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "node--teaser__field.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array();
        $filters = array();
        $functions = array();

        try {
            $this->env->getExtension('Twig_Extension_Sandbox')->checkSecurity(
                array(),
                array(),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 130
    public function block_field_icon($context, array $blocks = array())
    {
        echo "account_circle";
    }

    // line 131
    public function block_field_value($context, array $blocks = array())
    {
        // line 132
        echo "            <div class=\"teaser__published\">
              <div class=\"teaser__published-date\"> ";
        // line 133
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["date"] ?? null), "html", null, true));
        echo " &bullet; </div>
              <div class=\"teaser__published-author\"> ";
        // line 134
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["author_name"] ?? null), "html", null, true));
        echo " </div>
            </div>";
    }

    public function getTemplateName()
    {
        return "node--teaser.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  385 => 134,  381 => 133,  378 => 132,  375 => 131,  369 => 130,  327 => 129,  285 => 149,  283 => 146,  280 => 145,  277 => 144,  274 => 140,  272 => 139,  269 => 138,  267 => 129,  264 => 128,  261 => 127,  255 => 111,  251 => 110,  247 => 109,  243 => 108,  238 => 107,  235 => 106,  226 => 98,  221 => 95,  218 => 94,  210 => 188,  208 => 187,  198 => 180,  195 => 179,  191 => 177,  189 => 176,  187 => 175,  185 => 174,  182 => 173,  175 => 169,  167 => 167,  165 => 166,  162 => 165,  155 => 161,  146 => 156,  144 => 155,  138 => 151,  136 => 127,  130 => 125,  122 => 122,  117 => 121,  115 => 120,  111 => 119,  103 => 113,  100 => 106,  97 => 104,  94 => 103,  91 => 94,  89 => 93,  84 => 90,  78 => 87,  75 => 86,  73 => 85,  69 => 84,  66 => 83,  64 => 80,  63 => 79,  62 => 78,  61 => 77,  60 => 76,  59 => 75,  58 => 73,  53 => 70,  49 => 69,  46 => 68,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "node--teaser.html.twig", "profiles/contrib/social/themes/socialbase/templates/node/node--teaser.html.twig");
    }
}


/* node--teaser.html.twig */
class __TwigTemplate_f10e2cdf364e797187f6e0fb66dca3245ff453e7f2396406d4947ba8b9e21090_1071370171 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 140
        $this->parent = $this->loadTemplate("node--teaser__field.html.twig", "node--teaser.html.twig", 140);
        $this->blocks = array(
            'field_icon' => array($this, 'block_field_icon'),
            'field_value' => array($this, 'block_field_value'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "node--teaser__field.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array();
        $filters = array();
        $functions = array();

        try {
            $this->env->getExtension('Twig_Extension_Sandbox')->checkSecurity(
                array(),
                array(),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 141
    public function block_field_icon($context, array $blocks = array())
    {
        echo "group";
    }

    // line 142
    public function block_field_value($context, array $blocks = array())
    {
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["content"] ?? null), "group_name", array()), "html", null, true));
    }

    public function getTemplateName()
    {
        return "node--teaser.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  476 => 142,  470 => 141,  428 => 140,  385 => 134,  381 => 133,  378 => 132,  375 => 131,  369 => 130,  327 => 129,  285 => 149,  283 => 146,  280 => 145,  277 => 144,  274 => 140,  272 => 139,  269 => 138,  267 => 129,  264 => 128,  261 => 127,  255 => 111,  251 => 110,  247 => 109,  243 => 108,  238 => 107,  235 => 106,  226 => 98,  221 => 95,  218 => 94,  210 => 188,  208 => 187,  198 => 180,  195 => 179,  191 => 177,  189 => 176,  187 => 175,  185 => 174,  182 => 173,  175 => 169,  167 => 167,  165 => 166,  162 => 165,  155 => 161,  146 => 156,  144 => 155,  138 => 151,  136 => 127,  130 => 125,  122 => 122,  117 => 121,  115 => 120,  111 => 119,  103 => 113,  100 => 106,  97 => 104,  94 => 103,  91 => 94,  89 => 93,  84 => 90,  78 => 87,  75 => 86,  73 => 85,  69 => 84,  66 => 83,  64 => 80,  63 => 79,  62 => 78,  61 => 77,  60 => 76,  59 => 75,  58 => 73,  53 => 70,  49 => 69,  46 => 68,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "node--teaser.html.twig", "profiles/contrib/social/themes/socialbase/templates/node/node--teaser.html.twig");
    }
}


/* node--teaser.html.twig */
class __TwigTemplate_f10e2cdf364e797187f6e0fb66dca3245ff453e7f2396406d4947ba8b9e21090_1614396130 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 146
        $this->parent = $this->loadTemplate("node--teaser__body.html.twig", "node--teaser.html.twig", 146);
        $this->blocks = array(
            'field_body' => array($this, 'block_field_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "node--teaser__body.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array();
        $filters = array();
        $functions = array();

        try {
            $this->env->getExtension('Twig_Extension_Sandbox')->checkSecurity(
                array(),
                array(),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 147
    public function block_field_body($context, array $blocks = array())
    {
        echo " ";
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["content"] ?? null), "body", array()), "html", null, true));
        echo " ";
    }

    public function getTemplateName()
    {
        return "node--teaser.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  560 => 147,  519 => 146,  476 => 142,  470 => 141,  428 => 140,  385 => 134,  381 => 133,  378 => 132,  375 => 131,  369 => 130,  327 => 129,  285 => 149,  283 => 146,  280 => 145,  277 => 144,  274 => 140,  272 => 139,  269 => 138,  267 => 129,  264 => 128,  261 => 127,  255 => 111,  251 => 110,  247 => 109,  243 => 108,  238 => 107,  235 => 106,  226 => 98,  221 => 95,  218 => 94,  210 => 188,  208 => 187,  198 => 180,  195 => 179,  191 => 177,  189 => 176,  187 => 175,  185 => 174,  182 => 173,  175 => 169,  167 => 167,  165 => 166,  162 => 165,  155 => 161,  146 => 156,  144 => 155,  138 => 151,  136 => 127,  130 => 125,  122 => 122,  117 => 121,  115 => 120,  111 => 119,  103 => 113,  100 => 106,  97 => 104,  94 => 103,  91 => 94,  89 => 93,  84 => 90,  78 => 87,  75 => 86,  73 => 85,  69 => 84,  66 => 83,  64 => 80,  63 => 79,  62 => 78,  61 => 77,  60 => 76,  59 => 75,  58 => 73,  53 => 70,  49 => 69,  46 => 68,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "node--teaser.html.twig", "profiles/contrib/social/themes/socialbase/templates/node/node--teaser.html.twig");
    }
}
